# Changelog

## Unreleased

## 4.0.1 - 2023-12-15

### Changed

- #14 évolution du filtre `composition_class`

## 4.0.0 - 2023-11-21

### Changed

- #12 Compatibilité SPIP 4.1 mini

## Fixed

- #12 Retour de `compositions_verrou_branche()` toujours de type `bool`
