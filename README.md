# Compositions

Ce plugin vous permet de définir plusieurs variantes de compositions pour chaque type objet SPIP (article, rubrique, etc.) et de les appliquer simplement, cas par cas, depuis l’espace privé.

## Documentation

https://contrib.spip.net/3777