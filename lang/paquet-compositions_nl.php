<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-compositions?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// C
	'compositions_description' => 'Met deze plugin kun je meerdere composities creëren, voor elk type SPIP object (artikel, rubriek, enz.) en ze eenvoudig toepassen vanuit de privé-ruimte.',
	'compositions_nom' => 'Composities',
	'compositions_slogan' => 'Varieer de indeling van de bladzijdes volgens rubriek, artikel, ...',
];
