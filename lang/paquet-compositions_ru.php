<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-compositions?lang_cible=ru
// ** ne pas modifier le fichier **

return [

	// C
	'compositions_description' => 'Этот плагин позволяет вам задавать определенный шаблон вывода для конкретного раздела, статьи и т.д. ',
	'compositions_nom' => 'Шаблоны страниц (Compositions)',
	'compositions_slogan' => 'Возможность задавать определенный шаблон для вывода статьи, раздела и т.д.',
];
