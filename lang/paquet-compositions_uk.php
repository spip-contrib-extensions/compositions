<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-compositions?lang_cible=uk
// ** ne pas modifier le fichier **

return [

	// C
	'compositions_description' => 'Дозволяє використовувати різні шаблони для виводу статей, розділів та будь-яких об’єктів ',
	'compositions_nom' => 'Шаблони (Compositions)',
	'compositions_slogan' => 'Дозволяє використовувати різні шаблони для виводу статей, розділів та будь-яких об’єктів ',
];
