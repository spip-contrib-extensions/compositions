<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/compositions.git

return [

	// C
	'compositions_description' => 'Ce plugin vous permet de définir plusieurs variantes de compositions pour chaque type objet SPIP (article, rubrique, etc.) et de les appliquer simplement, cas par cas, depuis l’espace privé.',
	'compositions_nom' => 'Compositions',
	'compositions_slogan' => 'Varier les mises en page selon les rubriques, les articles...',
];
