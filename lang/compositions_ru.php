<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/compositions?lang_cible=ru
// ** ne pas modifier le fichier **

return [

	// A
	'ajouter_compositions' => 'Добавить шаблоны',

	// C
	'composition' => 'Шаблон страницы',
	'composition_defaut' => 'шаблон по умолчанию',
	'composition_heritee' => 'унаследован',
	'composition_utilisee' => 'Используется:',
	'composition_verrouillee' => 'Возможность сменить шаблон заблокирована вебмастером.',
	'compositions' => 'Шаблоны страниц',

	// D
	'des_utilisations' => '@nb@ используется',

	// H
	'heritages' => 'Этот шаблон определяет шаблоны страниц по умолчанию для следующих объектов:',

	// I
	'info_1_composition' => '1 шаблон',
	'info_aucune_composition' => 'Нет шаблонов',
	'info_nb_compositions' => '@nb@ шаблона',

	// L
	'label_activer_composition_objets' => 'Использовать выбор шаблонов для объектов',
	'label_branche_verrouillee' => 'Выбор шаблонов в этой ветке заблокирован вебмастером.',
	'label_chemin_compositions' => 'Папка с шаблонами',
	'label_chemin_compositions_details' => 'Укажите папку в которой находятся файлы шаблонов.',
	'label_composition' => 'Доступные шаблоны',
	'label_composition_branche_lock' => 'Заблокировать возможность менять шаблон для всех объектов в этой ветке',
	'label_composition_explication' => 'У вас права вебмастера, вы можете',
	'label_composition_lock' => 'Заблокировать шаблон',
	'label_composition_rubrique' => 'Шаблон для разделов',
	'label_heritages' => 'Наследование',
	'label_information' => 'Информация',
	'label_masquer_formulaire' => 'Спрятать форму',
	'label_masquer_formulaire_composition' => 'Скрывать форму выбора шаблона страницы для пользователей, у которых нет прав их менять.',
	'label_pas_de_composition' => 'Нет шаблонов',
	'label_styliser' => 'Выбор шаблонов страниц',
	'label_styliser_auto' => 'Не использовать автоматический выбор. Выбор поддерживается моими шаблонами.',
	'label_tout_verrouiller' => 'Заблокировать все шабоны',
	'label_toutes_verrouilles' => 'Все шаблоны заблокированы.',
	'label_verrouiller_toutes_compositions' => 'Заблокировать все шаблоны(только вебмастер сайта может их менять).',

	// M
	'message_info_ajouter_compositions' => 'Документация по созданию шаблонов : @url_doc@.',

	// U
	'une_utilisation' => '1 использование',

	// V
	'voir_image_exemple' => 'Посмотреть пример картинки',
];
