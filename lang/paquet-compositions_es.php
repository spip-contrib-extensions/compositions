<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-compositions?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// C
	'compositions_description' => 'Este plugin le permite definir diversas variantes de composiciones para cada tipo de objeto SPIP (artículo, sección, etcétera) y aplicarlas, simplemente, caso por caso, desde el espacio privado.',
	'compositions_nom' => 'Composiciones',
	'compositions_slogan' => 'Variar los formateos según las secciones, los artículos...',
];
