<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/compositions?lang_cible=uk
// ** ne pas modifier le fichier **

return [

	// A
	'ajouter_compositions' => 'Додати шаблони',

	// C
	'composition' => 'Шаблон',
	'composition_defaut' => 'шаблон за замовчуванням',
	'composition_heritee' => 'успадковано',
	'composition_utilisee' => 'Використовується:',
	'composition_verrouillee' => 'Вебмайстер заборонив змінювати цей шаблон',
	'compositions' => 'Шаблони',

	// D
	'des_utilisations' => '@nb@ застосувань',

	// H
	'heritages' => 'Наслідування, вказані у цьому шаблоні:',

	// I
	'info_1_composition' => '1 шаблон',
	'info_aucune_composition' => 'Шаблон відсутній',
	'info_nb_compositions' => '@nb@ шаблонів',

	// L
	'label_activer_composition_objets' => 'Дозволити використання шаблонів для наступних об’єктів',
	'label_branche_verrouillee' => 'У цьому розділі заборонено змінювати шаблони',
	'label_chemin_compositions' => 'Шаблони знаходяться у теці',
	'label_chemin_compositions_details' => 'Шлях до теки, де знаходяться файли шаблонів',
	'label_composition' => 'Вид шаблону',
	'label_composition_branche_lock' => 'Заборонити змінювати шаблони для всіх об’єктів в цьому розділі',
	'label_composition_explication' => 'Як Вебмайстер ви можете',
	'label_composition_lock' => 'Заборонити зміну шаблону',
	'label_composition_rubrique' => 'Шаблони розділів',
	'label_heritages' => 'Успадкування',
	'label_information' => 'Інформація',
	'label_masquer_formulaire' => 'Сховати форму',
	'label_masquer_formulaire_composition' => 'Приховати форму вибору шаблону, якщо користувач не має права змінювати його',
	'label_pas_de_composition' => 'Немає жодного шаблону',
	'label_styliser' => 'Вибір шаблонів',
	'label_styliser_auto' => 'Не призначати шаблон автоматично, необхідний шаблон вказаний у файлах.',
	'label_tout_verrouiller' => 'Заборонити зміну всіх шаблонів',
	'label_toutes_verrouilles' => 'Заборонена зміна всіх шаблонів',
	'label_verrouiller_toutes_compositions' => 'Заборонити змінювати всі шаблони (тільки вебмайстер зможе змінювати їх).',

	// M
	'message_info_ajouter_compositions' => 'Перед створенням власного шаблону - подивіться документацію: @url_doc@.',

	// U
	'une_utilisation' => '1 застосування',

	// V
	'voir_image_exemple' => 'Подивитися приклад зображення',
];
